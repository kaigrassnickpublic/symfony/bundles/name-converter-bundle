<?php
declare(strict_types=1);

/*
 * This file was part of the Symfony package. Copied from symfony/serializer
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace KaiGrassnick\NameConverterBundle\NameConverter;

/**
 * Class CamelCaseToSnakeCaseNameConverter
 *
 * @package KaiGrassnick\NameConverterBundle\NameConverter
 */
class CamelCaseToSnakeCaseNameConverter
{
    /**
     * @var array|null
     */
    private ?array $attributes;

    /**
     * @var bool
     */
    private bool $lowerCamelCase;


    /**
     * CamelCaseToSnakeCaseNameConverter constructor.
     *
     * @param array|null $attributes
     * @param bool       $lowerCamelCase
     */
    public function __construct(array $attributes = null, bool $lowerCamelCase = true)
    {
        $this->attributes     = $attributes;
        $this->lowerCamelCase = $lowerCamelCase;
    }


    /**
     * @param string $propertyName
     *
     * @return string
     */
    public function normalize(string $propertyName): string
    {
        if (null === $this->attributes || \in_array($propertyName, $this->attributes)) {
            return strtolower(preg_replace('/[A-Z]/', '_\\0', lcfirst($propertyName)));
        }

        return $propertyName;
    }


    /**
     * @param string $propertyName
     *
     * @return string
     */
    public function denormalize(string $propertyName): string
    {
        $camelCasedName = preg_replace_callback('/(^|_|\.)+(.)/', function ($match) {
            return ('.' === $match[1] ? '_' : '') . strtoupper($match[2]);
        }, $propertyName);

        if ($this->lowerCamelCase) {
            $camelCasedName = lcfirst($camelCasedName);
        }

        if (null === $this->attributes || \in_array($camelCasedName, $this->attributes)) {
            return $camelCasedName;
        }

        return $propertyName;
    }
}
