<?php declare(strict_types=1);

namespace KaiGrassnick\NameConverterBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class NameConverterBundle
 *
 * @package KaiGrassnick\NameConverterBundle
 */
class NameConverterBundle extends Bundle
{

}